import QtQuick 2.15
import QtQuick.Window 2.15
import Miko.Asuki 1.0

Window {
    width: Screen.width
    height: Screen.height
    visible: true
    flags: Qt.FramelessWindowHint | Qt.WindowStaysOnBottomHint | Qt.MaximizeUsingFullscreenGeometryHint
    title: "Background"
    color: "#440000"

    Settings {
        id: settings
    }

    Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: "file://" + settings.background
    }
}
