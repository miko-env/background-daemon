#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <KWindowSystem>


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    auto objects = engine.rootObjects();
    QQuickWindow* window = reinterpret_cast<QQuickWindow*>(objects[0]);
    KWindowSystem::setType(window->winId(), KWindowSystem::NET::WindowType::Desktop);

    return app.exec();
}
